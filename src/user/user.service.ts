import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './../Classes/user';
import { Details } from './../Classes/details';
import { History } from './../Classes/history';

@Injectable()
export class UserAPIService {
  UserId: Number;
  examId: Number;
  questionId: Number;
  dataDetails: Details[]=[];
  time : Number;

  setTimeExam(time:Number){
    this.time = time;
  }

  getTimeExam(){
    return this.time;
  }

  setQuestionId(id:Number){
    this.questionId = id;
  }

  getQuestionId(){
    return this.questionId;
  }

  setUserId(id:Number){
    this.UserId = id;
  }

  setExamId(id:Number)
  {
    this.examId = id;
  }

  URL: string="http://localhost:3000/user";
  detailURL:string ="http://localhost:3000/details";
  examURL: string ="http://localhost:3000/exam";
  historyURL: string = "http://localhost:3000/history";

  constructor(private httpClient: HttpClient) { }

  userUpdate(user: User):Observable<any>
  {
    return this.httpClient.put(this.URL+"/"+this.UserId, user);  
  }

  getDetailExamWithUserId():Observable<any>
  {
    console.log(this.detailURL+"/user/"+this.UserId);
    return this.httpClient.get(this.detailURL+"/user/"+this.UserId);
  }

  getExamWithExamId():Observable<any>
  {
    console.log(this.examURL+"/"+this.examId);
    return this.httpClient.get(this.examURL+"/"+this.examId);
  }

  getDetailExamWithExamIdAndUserId():Observable<any>
  {
    console.log(this.detailURL+"?"+"exam="+this.examId + "&user="+this.UserId);
    return this.httpClient.get(this.detailURL+"?"+"exam="+this.examId + "&user="+this.UserId);
  }

  setDetailsExam(dt: Details[])
  {
    this.dataDetails = dt
  }

  getDetailsExam(){
    return this.dataDetails;
  }
  
  postExamHistory(done:History):Observable<any>{
    return this.httpClient.post(this.historyURL, done);
  }

  getHistoryWithUserId():Observable<any>{
    console.log(this.historyURL+"/user/"+this.UserId)
    return this.httpClient.get(this.historyURL+"/user/"+this.UserId)
  }

  updateAnweserEssayWithUserIdAndExamIdAndQuestionId(awDetails: Details):Observable<any>
  {
    console.log(this.detailURL+"?user="+awDetails.user+"&exam="+awDetails.exam+"&question="+awDetails.questione)
    return this.httpClient.put(this.detailURL+"?user="+awDetails.user+"&exam="+awDetails.exam+"&question="+awDetails.questione, awDetails);
  }
}
