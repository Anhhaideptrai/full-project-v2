import { Component, OnInit } from '@angular/core';
import { UserAPIService } from './../user.service';
import { History }  from './../../Classes/history';
import { HomeAPIService } from './../../Home/home.service';
import { User } from './../../Classes/user';
import { Testes } from './../../Classes/testes';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  constructor(private api: UserAPIService, private user: HomeAPIService) { }

  exam= new Testes();
  historyData: History[];
  userLogin = new User();

  ngOnInit(): void {
    this.userLogin = this.user.getUserInfor();
    this.api.setUserId(this.userLogin.id)
    this.api.getHistoryWithUserId().subscribe
    (
      data=>
      {
        this.historyData = data;
        console.log(this.historyData);
      }
    )
    
  }  
}
