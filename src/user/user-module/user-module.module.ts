import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModules } from './../../routerModule/router/router.module';
import { MaterialModule } from './../../materialModule/material.module';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';
import { CountdownModule } from 'ngx-countdown';
import { HomeAPIService } from './../../Home/home.service';// get user Info when login

import { HttpClientModule } from '@angular/common/http';
import { UserAPIService } from './../user.service';

import { NavUserComponent } from './../../user/nav-user/nav-user.component';
import { NavBlogComponent } from './../../user/nav-blog/nav-blog.component';
import { HistoryComponent } from './../../user/history/history.component';
import { UserInforComponent } from './../../user/user-infor/user-infor.component';
import { UserTestChooseComponent } from './../../user/user-test-choose/user-test-choose.component';
import { DoTestComponent } from './../../user/do-test/do-test.component';

@NgModule({
  declarations: [ 
    NavUserComponent,
    NavBlogComponent,
    HistoryComponent,
    UserInforComponent,
    UserTestChooseComponent,
    DoTestComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    RouterModules,
    LayoutModule,
    CountdownModule,
    HttpClientModule 
  ],
  providers:[UserAPIService, HomeAPIService],
})
export class UserModuleModule { }
