import { Component, OnInit } from '@angular/core';
import { Details } from './../../Classes/details';
import { UserAPIService } from './../user.service';
import { HomeAPIService } from './../../Home/home.service';// get user Info when login
import { User } from './../../Classes/user';
import { Testes } from './../../Classes/testes';

@Component({
  selector: 'app-user-test-choose',
  templateUrl: './user-test-choose.component.html',
  styleUrls: ['./user-test-choose.component.css']
})
export class UserTestChooseComponent {

  panelOpenState = false;
  user = new User();

  constructor(private api: UserAPIService, private getUserInforAPI: HomeAPIService) {  }

  detailsData: Details[];
  examData: Testes[] =[];

  ngOnInit() {
    this.user = this.getUserInforAPI.getUserInfor();
    this.api.setUserId(this.user.id);

    this.api.getDetailExamWithUserId().subscribe(
      data =>{ this.detailsData = data}
    );
  }
find_Click()
  {
    var grades = {};
    this.detailsData.forEach( function( item ) {
    var grade = grades[item.user] = grades[item.user] || {};
    grade[item.exam] = true;
    });

    var outputList = [];
    for(var grade in grades)
    {
      for(var i in grades[grade])
      {
        outputList.push(i)
      }
    }

    for(let i =0 ; i< outputList.length; i++)
    {
      this.api.setExamId(outputList[i]);
      this.api.getExamWithExamId().subscribe(data =>{this.examData.push(data); this.api.setTimeExam(data.time)})
    }
  }
  newdetailData: Details[]=[];
  getExam(i: Testes){  
    this.api.setUserId(this.user.id);
    this.api.setExamId(i.id);
    this.api.getDetailExamWithExamIdAndUserId().subscribe(data=>{this.newdetailData.push(data); })
    this.api.setDetailsExam(this.newdetailData);
  }

}
