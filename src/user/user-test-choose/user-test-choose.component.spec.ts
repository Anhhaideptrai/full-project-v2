import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTestChooseComponent } from './user-test-choose.component';

describe('UserTestChooseComponent', () => {
  let component: UserTestChooseComponent;
  let fixture: ComponentFixture<UserTestChooseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserTestChooseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTestChooseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
