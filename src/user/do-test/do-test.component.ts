import { Component, OnInit } from '@angular/core';
import { UserAPIService } from './../user.service';
import { Details } from './../../Classes/details';
import { History } from './../../Classes/history';

@Component({
  selector: 'app-do-test',
  templateUrl: './do-test.component.html',
  styleUrls: ['./do-test.component.css']
})
export class DoTestComponent implements OnInit {
  
detailsExam: Details[]
public files: any[];

constructor(private api: UserAPIService) { this.files = [];  }

ngOnInit(): void {
  this.detailsExam = this.api.getDetailsExam();
  this.userId = this.api.UserId;
  this.examId = this.api.examId;
  this.timedata = Number(this.api.getTimeExam())*60;
}
userId;
examId;
url;
//bind timetest
timedata: Number;
isShowQuizz="";
showEssay="";
showQuizzResult="";
showEssayResult="";
state_: number;
state = "none";
hidden = "";
i = 0;
Anweser: string;
arrAnweser: string[]=[];
NUMBER_OF_QUESTION: number;
id=0;
content="";
solutionA="";
solutionB="";
solutionC="";
solutionD="";
Aws ="";
date = new Date()
data: Details[]=[];
point:number=0;
history = new History();
detailUpdate = new Details();

showQuestion = (index)=>{
  let quizz = this.detailsExam[index];

  for(let q in quizz){
    if(quizz.hasOwnProperty(q)){
      this.data.push(quizz[q])
    }
  } 
    this.id = this.data[this.i].question.id;
    this.content = this.data[this.i].question.content;
    this.solutionA = this.data[this.i].question.solutionA;
    this.solutionB = this.data[this.i].question.solutionB;
    this.solutionC = this.data[this.i].question.solutionC;
    this.solutionD = this.data[this.i].question.solutionD;
    this.Aws = this.data[this.i].question.correct;

  // console.log(this.data.length)
}
startQuizz_Fnc(){
    this.state = "flex";
    this.hidden = "none";
    this.showQuestion(this.i);

    //CHECK TYPE OF TEST
  if( this.Aws == null && this.solutionA == null)
  {
    this.isShowQuizz="none";
    this.showEssay="";
    this.state_ =1;
  }
  else{
    this.isShowQuizz="";
    this.showEssay="none";
    this.state_ =0;
  }
  //---------------------
    this.NUMBER_OF_QUESTION = this.data.length;
}

onSelectFile(event : any) { // called each time file input changes
  if (event.target.files && event.target.files[0]) {
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]); // read file as data url
    reader.onload = (event) => { // called once readAsDataURL is completed
      this.url = event.target.result;
        }
      }
    this.files = event.target.files;
  }


nextquestion_fnc(anw: string){
  if(this.state_ ==0)
  {
    //show quizz exam
    if(this.Anweser != undefined){
      this.arrAnweser.push(this.Anweser);    
    }else{
      this.arrAnweser.push(null)
    }   
    if(this.Anweser == this.Aws){
      this.point = this.point+1
    }
    // console.log(this.Anweser+"--"+this.Aws + "---"+this.point)
    this.i=this.i+1;
    if(this.i == this.data.length)
    {
      this.i = this.data.length-1
      if(window.confirm("Submit Your Test")){
        this.state ="none";
        this.isShow= "flex";
        this.showEssayResult="none";
      }
    }
    this.showQuestion(this.i)
  } 
  else{
    //show essay exam
    // console.log(this.userId+"--"+this.examId+"--"+this.id+"--"+anw);
    // const formData = new FormData();
    // for (const file of this.files) {
    //     formData.append(name, file, file.name);
    // }
    this.url="";
    this.detailUpdate.user = this.userId;
    this.detailUpdate.exam = this.examId;
    this.detailUpdate.questione = this.id;
    this.detailUpdate.selectedAnswer = anw;
    this.api.updateAnweserEssayWithUserIdAndExamIdAndQuestionId(this.detailUpdate).subscribe();
    this.i=this.i+1;
    if(this.i == this.data.length)
    {
      this.i = this.data.length-1
      if(window.confirm("Submit Your Test")){
        this.state ="none";
        this.isShow= "flex";
        this.showQuizzResult="none";
      }
    }
    this.showQuestion(this.i)
  }   
}

isShow="none"
submitTest_fnc(){
  if(this.state_ == 0)
  {
    if(window.confirm("Are you sure?")){
      this.state ="none";
      this.isShow= "flex";
      this.showQuizzResult = "";
      this.showEssayResult="none";
    }
  }else{
    if(window.confirm("Are you sure?")){
      this.state ="none";
      this.isShow= "flex";
      this.showQuizzResult = "none";
      this.showEssayResult="";
    }
  }    
}

back_fnc(){
  this.state ="none";
  this.isShow= "none";
  this.hidden="";
  if(this.state_ == 0)
  {   
    this.history.date = this.date;
    this.history.result = String(this.point)+"/"+String(this.NUMBER_OF_QUESTION);
    this.history.user = this.userId;
    this.history.exam = this.examId;
    console.log(this.history)
    this.api.postExamHistory(this.history).subscribe(data=>{data});
  }else{
    this.history.date = this.date;
    this.history.result = "submitted, waiting...";
    this.history.user = this.userId;
    this.history.exam = this.examId;
    console.log(this.history)
    this.api.postExamHistory(this.history).subscribe(data=>{data});
  }   
  }
}
