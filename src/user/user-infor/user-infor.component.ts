import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
  FormGroup
} from '@angular/forms';
import { HomeAPIService } from './../../Home/home.service';// get user Info when login
import { User } from './../../Classes/user';
import { UserAPIService } from './../user.service';

@Component({
  selector: 'app-user-infor',
  templateUrl: './user-infor.component.html',
  styleUrls: ['./user-infor.component.css'],
})
export class UserInforComponent implements OnInit {

  constructor(private getUserInforAPI: HomeAPIService, private userAPI: UserAPIService) { }

  UserInfo = new User();
  informations: User[] = [];
  show = true;
  newInfo= new User();
  hidden = "none";
  pwState ="none";
  formState="";
  error:string;

  ngOnInit(): void {
    this.UserInfo = this.getUserInforAPI.getUserInfor();
    this.informations.push(this.UserInfo);
    console.log(this.informations[0].id)
  }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  //btn-submit
  getValueInfo(id, name,email, phone, address, password ) {
    this.newInfo = {id:id, name: name, password: password,
      phone: phone, address: address, email: email,authentication: this.informations[0].authentication,role:false };
    // console.log(this.newInfo);
    this.userAPI.setUserId(id);
    this.userAPI.userUpdate(this.newInfo).subscribe
    (
      data =>{data}
    );
    alert("Updated!!!")
  }

  form = new FormGroup({
    newPW: new FormControl('', [Validators.required, Validators.minLength(7)])

  })

  comfirmPassword(id, password, newpw ){
    // console.log(newpw)
    if(this.form.valid){
      this.newInfo.password = newpw
      // console.log(id+"---"+password+"---"+newpw)
      password = newpw;
      this.formState="";
      this.pwState="none";
      this.userAPI.setUserId(id);
      this.userAPI.userUpdate(this.newInfo).subscribe (
        data =>{data}
      );
    }else{
      console.log("faild")
    } 
  }
  changePassword(){
    this.pwState="";
    this.formState="none";
  }

}
