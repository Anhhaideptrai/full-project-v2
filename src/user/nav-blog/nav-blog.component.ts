import { Component, OnInit } from '@angular/core';
import {UserAPIService} from './../user.service'; 

@Component({
  selector: 'app-nav-blog',
  templateUrl: './nav-blog.component.html',
  styleUrls: ['./nav-blog.component.css']
})
export class NavBlogComponent implements OnInit {

  constructor(private api: UserAPIService) { }

  ngOnInit(): void {
  }
  
}
