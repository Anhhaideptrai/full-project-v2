export class Exam {
    public id: number;
    public questionNumber: number;
    public time: number;
    public level: number;
    public field: {
        id: number,
        major: string
    };
}