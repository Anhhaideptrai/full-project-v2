export class Quizzes {
    public id : number;
    public content: string;
    public solutionA: string;
    public solutionB: string;
    public solutionC: string;
    public solutionD: string;
    public correct: string;
    public field: {
        id: number,
        major: string,
    };
    public level: {
        id: number,
        label: string
    }
}
