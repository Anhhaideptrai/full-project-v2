export class HistoryAdmin{
    id: number;
    date: Date;
    result: string;
    user: string;
    exam: string;
    name: string;
    field: string;
}