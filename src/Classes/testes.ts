export class Testes {
    id: Number;
    questionNumber: Number;
    time: Number;
    field: {id: number, major: string};
    level: {id:number,label: string};
}
