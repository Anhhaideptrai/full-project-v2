import { Component, OnInit } from '@angular/core';
import { User } from '../../../Classes/user';
import {Quizzes} from '../../../Classes/quizzes';
import {AdminService} from '../../admin.service';
import {Details} from '../../../Classes/details';

@Component({
  selector: 'app-add-test',
  templateUrl: './add-test.component.html',
  styleUrls: ['./add-test.component.css']
})
export class AddTestComponent implements OnInit {
  isSelected = false;
  questions:Quizzes[] = [];
  questionFilter:Quizzes[] = [];
  questionsSelect:Quizzes[] = [];
  users: User[] = [];
  userSelect: User[] = [];
  usersFilter: User[] = [];
  data = [];
  field = 0;
  level = "Select";
  soluong = 0;
  userView = 3;
  show = false;
  username: "";
  exam: number;
  examData:[] = [];
  selectAll = false;
  constructor(private adminService: AdminService) { }
  ngOnInit(): void {
    this.adminService.getQuestion().subscribe(data =>
      {this.questions = data;
       console.log(this.questions);
      });
    this.adminService.getUser().subscribe(data => this.users = data);
  }
  filter(){
    if(this.level == "Select" ){
      this.questionFilter = this.questions.map(res => res);
      this.questionsSelect = [];
      this.soluong = 0;
    }
    else if(this.level === "1" ){
       this.questionFilter = this.questions.filter(res => {
         return res.level.id == 1;
       });
       this.questionsSelect = [];
       this.soluong = 0;
    }
     else if(this.level == "2"){
      this.questionFilter = this.questions.filter(res => res.level.id == 2);
      this.questionsSelect = [];
      this.soluong = 0;
    }
    else if(this.level == "3"){
      this.questionFilter = this.questions.filter(res => res.level.id == 3);
      this.questionsSelect = [];
      this.soluong = 0;
    }
  }
  filter1(){
    if(this.field == 0 && this.level == "1"){
      this.questionFilter = this.questions.filter(res => {
        return res.level.id == 1;
      });
      this.questionsSelect = [];
      this.soluong = 0;
    }
    if(this.field == 0 && this.level == "2"){
      this.questionFilter = this.questions.filter(res => {
        return res.level.id == 2;
      });
      this.questionsSelect = [];
      this.soluong = 0;
    }
    if(this.field == 0 && this.level == "3"){
      this.questionFilter = this.questions.filter(res => {
        return res.level.id == 3;
      });
      this.questionsSelect = []; 
      this.soluong = 0;
    }
    if(this.field == 1 &&this.level == "1"){
      this.questionFilter = this.questions.filter(res => res.field.id == 1 && res.level.id == 1);
      this.questionsSelect = [];
      this.soluong = 0;
   }
   else if(this.field == 2 &&this.level == "1"){
    this.questionFilter = this.questions.filter(res => res.field.id == 2&& res.level.id == 1);
    this.questionsSelect = [];
    this.soluong = 0;
   }
   else if(this.field == 3&&this.level == "1"){
    this.questionFilter = this.questions.filter(res => res.field.id == 3&& res.level.id== 1);
    this.questionsSelect = [];
    this.soluong = 0;
   }
   else if(this.field == 4&&this.level == "1"){
    this.questionFilter = this.questions.filter(res => res.field.id == 4&& res.level.id== 1);
    this.questionsSelect = [];
    this.soluong = 0;
   }
   else if(this.field == 5&&this.level == "1"){
    this.questionFilter = this.questions.filter(res => res.field.id == 5&& res.level.id== 1);
    this.questionsSelect = [];
    this.soluong = 0;
   }
   else if(this.field == 6&&this.level == "1"){
    this.questionFilter = this.questions.filter(res => res.field.id == 6&& res.level.id== 1);
    this.questionsSelect = [];
    this.soluong = 0;
   }
   else if(this.field == 1&&this.level == "2"){
    this.questionFilter =  this.questions.filter(res => res.field.id == 1 && res.level.id == 2);
    this.questionsSelect = [];
    this.soluong = 0;
    }
    else if(this.field == 2&&this.level == "2"){
      this.questionFilter =  this.questions.filter(res => res.field.id == 2 && res.level.id == 2);
      this.questionsSelect = [];
      this.soluong = 0;
      }
    else if(this.field == 3 &&this.level == "2"){
      this.questionFilter = this.questions.filter(res => res.field.id == 3 && res.level.id== 2);
      this.questionsSelect = [];
      this.soluong = 0;
     }
     else if(this.field == 4 &&this.level == "2"){
      this.questionFilter = this.questions.filter(res => res.field.id == 4 && res.level.id== 2);
      this.questionsSelect = [];
      this.soluong = 0;
     }
     else if(this.field == 5 &&this.level == "2"){
      this.questionFilter = this.questions.filter(res => res.field.id == 5 && res.level.id== 2);
      this.questionsSelect = [];
      this.soluong = 0;
     }
     else if(this.field == 6 &&this.level == "2"){
      this.questionFilter = this.questions.filter(res => res.field.id == 6 && res.level.id== 2);
      this.questionsSelect = [];
      this.soluong = 0;
     } 
     else if(this.field == 1&&this.level == "3"){
      this.questionFilter =  this.questions.filter(res => res.field.id == 1 && res.level.id == 3);
      this.questionsSelect = [];
      this.soluong = 0;
      }
      else if(this.field == 2&&this.level == "3"){
        this.questionFilter =  this.questions.filter(res => res.field.id == 2 && res.level.id == 3);
        this.questionsSelect = [];
        this.soluong = 0;
        }
      else if(this.field == 3 &&this.level == "3"){
        this.questionFilter = this.questions.filter(res => res.field.id == 3 && res.level.id== 3);
        this.questionsSelect = [];
        this.soluong = 0;
       }
       else if(this.field == 4 &&this.level == "3"){
        this.questionFilter = this.questions.filter(res => res.field.id == 4 && res.level.id== 3);
        this.questionsSelect = [];
        this.soluong = 0;
       }
       else if(this.field == 5 &&this.level == "3"){
        this.questionFilter = this.questions.filter(res => res.field.id == 5 && res.level.id==3);
        this.questionsSelect = [];
        this.soluong = 0;
       }
       else if(this.field == 6 &&this.level == "3"){
        this.questionFilter = this.questions.filter(res => res.field.id == 6 && res.level.id== 3);
        this.questionsSelect = [];
        this.soluong = 0;
       }  
  }
  exist(item: any){
    console.log(this.questionsSelect.indexOf(item) > -1);
    return this.questionsSelect.indexOf(item) > -1;
  }
  toggleSelection(item: any){
    var idx = this.questionsSelect.indexOf(item);
    console.log(idx)
    if(idx > -1){
      this.questionsSelect.splice(idx,1)
      this.soluong --;
    }
    else{
      this.questionsSelect.push(item);
      this.soluong ++;
      console.log(this.questionsSelect)
    }
  }
  checkAll(){
    if(this.selectAll == false){
     for(var i=0;i<this.questionFilter.length;i++){
        this.questionsSelect.push(this.questionFilter[i]);
     }
   }
    else{
       this.questionsSelect = [];
    }
    this.soluong = this.questionsSelect.length;
  }
  // oneToRight(e){
  //   e.preventDefault();
  //   var select = (document.getElementById('list1') as HTMLSelectElement).value  
  //   if(select != ""){
  //     for(var i=0;i<this.questionFilter.length;i++){
  //       if(this.questionFilter[i].content.trim() == select){ 
  //         this.questionsSelect.push(this.questionFilter[i])
  //         console.log(this.questionFilter[i])
  //         var del = this.questionFilter.indexOf(this.questionFilter[i])
  //         this.questionFilter.splice(del,1)         
  //       }         
  //     }
  //   }
  //   this.soluong = this.questionsSelect.length;
  // }
  // oneToLeft(e){
  //   e.preventDefault();
  //   var select = (document.getElementById('list2') as HTMLSelectElement).value  
  //   if(select != ""){
  //     for(var i=0;i<this.questionsSelect.length;i++){
  //       if(this.questionsSelect[i].content.trim() == select){
  //         this.questionFilter.push(this.questionsSelect[i])
  //         console.log(this.questionsSelect[i])
  //         var del = this.questionsSelect.indexOf(this.questionsSelect[i])
  //         this.questionsSelect.splice(del,1) 
  //       }
  //     }
  //   }
  //   this.soluong = this.questionsSelect.length;
  // }
  // allToRight(e){
  //   e.preventDefault();
  //   for(var i=0;i<this.questionFilter.length;i++){
  //       this.questionsSelect.push(this.questionFilter[i])
  //       console.log(this.questionFilter[i])
  //       var del = this.questionFilter.indexOf(this.questionFilter[i])
  //       this.questionFilter.splice(del,1)         
  //     i--       
  //   }
  //   this.soluong = this.questionsSelect.length;
  // }
  // allToLeft(e){
  //   e.preventDefault();
  //   for(var i=0;i<this.questionsSelect.length;i++){
  //       this.questionFilter.push(this.questionsSelect[i])
  //       var del = this.questionsSelect.indexOf(this.questionsSelect[i])
  //       this.questionsSelect.splice(del,1)         
  //     i--       
  //   }
  //   this.soluong = this.questionsSelect.length;
  // }
  Search(){
    if(this.username == ""){
      this.ngOnInit();
    }
    else{
      this.users = this.users.filter(res =>{
        return res.name.toLocaleLowerCase().match(this.username.toLocaleLowerCase());
      } );
    }
  }
  asignUser(){
   var select = (document.getElementById('listUser') as HTMLSelectElement).value  
    if(select != ""){
      for(var i=0;i<this.users.length;i++){
        if(this.users[i].name.trim() === select){ 
          this.userSelect.push(this.users[i])
          var del = this.users.indexOf(this.users[i])
          this.users.splice(del,1)       
        }         
      }
      i--;
    }
  }
  unasignUser(){
    var select = (document.getElementById('listUser2') as HTMLSelectElement).value  
    if(select != ""){
      for(var i=0;i<this.userSelect.length;i++){
        if(this.userSelect[i].name.trim() === select){ 
          this.users.push(this.userSelect[i])
           var del = this.userSelect.indexOf(this.userSelect[i])
          this.userSelect.splice(del,1)       
        }         
      }
      i--;
    }
  }
  onSubmit(formSignIn){
    if(formSignIn.form.controls.time.value <= 0){
      alert("Please Type Time For Exam Correct Format");
      return;
    }
    if(formSignIn.form.controls.field.value == 0){
      alert("Please Type Field");
      return;
    }
    if(formSignIn.form.controls.level.value == 0){
      alert("Please Type Level");
      return;
    }
    if(this.userSelect.length == 0){
      alert("Please Assign User");
      return;
    }
    if(this.questionsSelect.length == 0){
      alert("Please Choose Questione");
      return;
    }
    this.adminService.postExam(formSignIn.value).subscribe(res => {
      for(let i=0; i < this.userSelect.length; i++){
      for(let j = 0; j <this.questionsSelect.length ; j++){
        let obj = {
          "user": this.userSelect[i].id,
          "exam": res.data.id,
          "question": this.questionsSelect[j].id,
        };
        this.data.push(obj);
      }
    }
    window.location.href = "/admin/manageTest";
    this.adminService.postDetail(this.data).subscribe();
    });
   
  }
}
