import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/admin/admin.service';
import {ActivatedRoute,ParamMap} from '@angular/router';
import {Exam} from '../../../Classes/exam';
import {Quizzes} from '../../../Classes/quizzes';
@Component({
  selector: 'app-edit-test',
  templateUrl: './edit-test.component.html',
  styleUrls: ['./edit-test.component.css']
})
export class EditTestComponent implements OnInit {
  time = "";
  questionNumber = null;
  field = "";
  level = "";
  fiedldId= null;
  levelId = null;
  id = "";
  data: Exam[]= [];
  dataQX = [];
  dataQ = [];
  rs = [];
  deleteQuestions = [];
  insertQuestion = [];
  

  show = false;
  selectAll = false;
  questioneEdit = [];
  constructor(private adminService: AdminService,private route: ActivatedRoute) { }
  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) =>{
      this.id = params.get('id');
      this.adminService.getOneExam(this.id).subscribe(data =>{
        this.data = data;
        this.time = data.time;
        this.questionNumber = data.questionNumber;
        this.level = data.level.label;
        this.field = data.field.major;
        this.fiedldId = data.field.id;
        this.levelId = data.level.id;
        this.adminService.getQuestionByExamId(this.id).subscribe(e =>{
          this.rs = e ;
        }); 
        this.adminService.getQuestionByFieldAndLevel(this.fiedldId,this.levelId).subscribe(dt => {
          this.dataQ = dt;
          console.log(this.dataQ);
        })     
      });
    });
  }
  toggleSelection(item: any){
    var idx = this.rs.indexOf(item);
    console.log(idx)
    if(idx > -1){
      this.rs.splice(idx,1);
      this.deleteQuestions.push(item);
      this.questionNumber --;
    }
    else{
      this.questionNumber ++;
    }
  }
  toggle(item: any){
    var idx = this.dataQ.indexOf(item);
    console.log(idx)
    if(idx > -1){
      if(this.rs.filter(res => res.content === item.content).length > 0)
      {
        alert("Ton Tai Cau hoi Vui Long Kiem Tra Lai!!!");
        return;
      }
      else{
      this.insertQuestion.push(item);
      this.rs.push(item);
      this.dataQ.splice(idx,1);
      this.questionNumber ++;
      } 
    }
    else{
      this.questionNumber --;
    }
  }
  onSubmit(formSignIn){
    console.log(this.rs);
    console.log(this.insertQuestion);
    console.log(this.deleteQuestions);
    console.log(this.id);
    console.log(formSignIn.value);
    this.adminService.putExam(this.id,formSignIn.value).subscribe();
    const deleteIDs = this.deleteQuestions.reduce((acc,cur)=>{
        return [...acc,cur.id];
    },[]);
     this.adminService.putDetails(this.id,this.insertQuestion,deleteIDs).subscribe();
     window.location.href = "/admin/manageTest";
  }
}
