import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../admin.service';
import {Exam} from '../../../Classes/exam';
import {Details} from '../../../Classes/details';
import { RouterLink } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
@Component({
  selector: 'app-manage-test',
  templateUrl: './manage-test.component.html',
  styleUrls: ['./manage-test.component.css']
})
export class ManageTestComponent implements OnInit {
  exams:Exam[] = [];
  details: Details[]= [];
  detailsDelete: Details[]= [];
  p = null;
  search = "";
  histories = [];
  constructor(private adminService:AdminService ) { }

  ngOnInit(): void {
    this.adminService.getExam().subscribe(data =>{
      this.exams = data
    }); 
  }
  deleteExam(id: number){
    this.adminService.getDetail().subscribe(res =>{
      this.details = res;
      this.detailsDelete = this.details.filter(res => res.exam == id);
      if(this.detailsDelete.length){
        alert("This test is currently being tested by someone please Try Agian!!!!");
        return;
      }
       window.confirm(" Coffirm Delete?");
       this.adminService.deleteExam(id).subscribe();
       window.location.href="/admin/manageTest";     
     })
  }
  Search(){
    if(this.search == ""){
      this.ngOnInit();
    }
    else{
      this.exams = this.exams.filter(res =>{
        return res.field.major.toLocaleLowerCase().match(this.search.toLocaleLowerCase());
      } );
    }
  }
  CheckDelete(id: string){
    this.adminService.getHistory().subscribe(data =>{
      this.histories = data;
      if(this.histories.filter(rs => rs.exam == id).length > 0){
        alert("The Exam Now Has Human Active, Try Agian");
        return;
      }
      else
         window.location.href="/admin/editTest/"+id;
    })
    
  }
   
     
  

}
