import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModules } from './../../routerModule/router/router.module';
import { MaterialModule } from './../../materialModule/material.module';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';

import { AdminComponent } from './../admin.component';
import { ManageAccountsComponent } from './../../admin/accounts/manage-accounts/manage-accounts.component';
import { EditAccountComponent } from './../../admin/accounts/edit-account/edit-account.component';
import { DeleteAccountComponent } from './../../admin/accounts/delete-account/delete-account.component';
import { AddAccountComponent } from './../../admin/accounts/add-account/add-account.component';
import { ManageTestComponent } from './../../admin/test/manage-test/manage-test.component';
import { AddTestComponent } from './../../admin/test/add-test/add-test.component';
import { EditTestComponent } from './../../admin/test/edit-test/edit-test.component';
import { DeleteTestComponent } from './../../admin/test/delete-test/delete-test.component';
import { ManageQuestionsComponent } from '../../admin/questions/manage-questions/manage-questions/manage-questions.component';
import { AddQuestionComponent } from '../../admin/questions/add-question/add-question.component';
import { EditQuestionComponent } from '../../admin/questions/edit-question/edit-question.component';
import { DeleteQuestionComponent } from '../../admin/questions/delete-question/delete-question.component';
import {HistoryComponentAdmin} from '../../admin/history/history.component';
import {MarkComponent} from '../history/mark/mark.component';
import {AdminService} from '../admin.service';
import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2OrderModule } from 'ng2-order-pipe';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
@NgModule({
  declarations: [ 
    AdminComponent, 
    ManageAccountsComponent, 
    EditAccountComponent, 
    DeleteAccountComponent,
    AddAccountComponent,
    ManageTestComponent,
    AddTestComponent,
    EditTestComponent,
    DeleteTestComponent,
    ManageQuestionsComponent,
    AddQuestionComponent,
    EditQuestionComponent,
    DeleteQuestionComponent,
    HistoryComponentAdmin,
    MarkComponent
   ],
   
  imports: [
    CommonModule,
    RouterModules,
    MaterialModule,
    ReactiveFormsModule, 
    FormsModule,
    NgxPaginationModule,
    Ng2OrderModule,
    Ng2SearchPipeModule
  ],
  providers:[
    AdminService
  ]
})
export class AdminModule { }
