
import { AfterViewInit,Component, OnInit ,ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {Router} from '@angular/router'
import {AdminService} from '../../admin.service' ;
import { User} from '../../../Classes/user';
import {ActivatedRoute,ParamMap} from '@angular/router';
import { RouterModule, Routes } from '@angular/router';
@Component({
  selector: 'app-manage-accounts',
  templateUrl: './manage-accounts.component.html',
  styleUrls: ['./manage-accounts.component.css']
})

export class ManageAccountsComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  accounts: User[] = [];
  id = '';
  username: any;
  email: any;
  
  constructor(private breakpointObserver: BreakpointObserver ,private AdminAPI: AdminService,private route: ActivatedRoute,private router: Router) {}
  ngOnInit(): void {
    this.AdminAPI.getUser().subscribe(
      data => {this.accounts = data}
    )
  }
  deleteAccount(idDel: string){
      this.AdminAPI.delUser(idDel).subscribe();
      if(window.confirm(" Coffirm Delete?"))
        window.onload;
  }
  Search(){
    if(this.username == ""){
      this.ngOnInit();
    }
    else{
      this.accounts = this.accounts.filter(res =>{
        return res.name.toLocaleLowerCase().match(this.username.toLocaleLowerCase());
      } );
    }
  }
 
}




