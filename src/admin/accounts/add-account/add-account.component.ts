import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {AdminService} from '../../admin.service' ;
import { User} from '../../../Classes/user';
import { ReturnStatement } from '@angular/compiler';
@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css']
})
export class AddAccountComponent implements OnInit {
  error = false;
  constructor(private adminService: AdminService,private router: Router) { }
  accounts: User[] = [];
  ngOnInit(): void {
    this.adminService.getUser().subscribe(
      data => {this.accounts = data}
    );
  }
  user1 = new User();
  
  onSubmit(formSignIn){
    var exitsUser = this.accounts.filter(account => account.name == formSignIn.form.controls.name.value.trim());
    if(exitsUser.length){
      this.error = true;
      return; 
      
    }
    this.adminService.postUser(formSignIn.value).subscribe();
    this.error = false;
    window.location.href='/admin/manageAccounts';
   }
}
