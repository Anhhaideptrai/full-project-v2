import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,ParamMap} from '@angular/router'
import { from } from 'rxjs';
import {AdminService} from '../../admin.service';
import {User} from '../../../Classes/user'
@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.css']
})
export class EditAccountComponent implements OnInit {
  id = '';
  constructor(private route: ActivatedRoute,private adminService: AdminService) { }
  account:User;
  name = "";
  password = "";
  phone = "" ;
  address = "";
  email = "";
  authentication = null ;
  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) =>{
      this.id = params.get('id');
      this.adminService.getOne(this.id).subscribe(data =>{
        this.account = data;
        this.name = this.account.name;
        this.password = this.account.password;
        this.phone = this.account.phone;
        this.address = this.account.address;
        this.email = this.account.email;
        this.authentication = this.account.authentication;
        
      });
    });
  }
  onSubmit(formSignIn){
    this.adminService.putUser(formSignIn.value,this.id).subscribe();
    window.location.href='/admin/manageAccounts';
  }
  
}
