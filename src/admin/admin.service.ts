import {User} from '../Classes/user';
import {Exam} from '../Classes/exam';
import { HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import {Quizzes} from '../Classes/quizzes';
@Injectable()
export class AdminService {
  
  URL: string="http://localhost:3000/user";
  URLQ: string="http://localhost:3000/question";
  URLQS: string="http://localhost:3000/question/many";
  URLE: string="http://localhost:3000/exam";
  URLDE: string="http://localhost:3000/details";
  URLD: string="http://localhost:3000/details/many";
  URLF: string = "http://localhost:3000/field";
  URLL: string = "http://localhost:3000/level";
  URLH: string = "http://localhost:3000/history"
  urlt: string = "http://localhost:3000/question?field=";
  urlt1: string =  "&level=";
  
  constructor(private httpClient: HttpClient) { }
  getUser():Observable<any>
  {
    return this.httpClient.get(this.URL);
  }
  postUser(user: {}={}):Observable<any>
  {
    return this.httpClient.post(this.URL,user)
  }
  getOne(id: string):Observable<any>
  {
    return this.httpClient.get("http://localhost:3000/user/"+ id);
  }
  putUser(user: {}={},id:string):Observable<any>
  {
    const url = `${this.URL}/${id}`
    return this.httpClient.put(url,user)
  }
  delUser(id:string):Observable<any>
  {
    const url = `${this.URL}/${id}`;
    return this.httpClient.delete(url);
  }
  /// Manage Question 
  getQuestion():Observable<any>
  {
    return this.httpClient.get(this.URLQ);
  }
  getOneQuesion(id: string):Observable<any>
  {
    return this.httpClient.get("http://localhost:3000/question/"+ id);
  }
  postQuestions(questions: any):Observable<any>
  {
    return this.httpClient.post(this.URLQS,questions)
  }
  putQuestion(question: any,id:string):Observable<any>
  {
    const url = `${this.URLQ}/${id}`;
    return this.httpClient.put(url,question);
  }
  delQuestion(id:string):Observable<any>
  {
    const url = `${this.URLQ}/${id}`;
    return this.httpClient.delete(url);
  }
  getQuestionByFieldAndLevel(field:number,level:number):Observable<any>
  {
    const url = "http://localhost:3000/question?field='"+field+"'&level='"+level+"'";
    const url1 = `${this.urlt}${field}${this.urlt1}${level}`
    return this.httpClient.get(url1);
  }
  getQuestionByExamId(id: string):Observable<any>
  {
    const url = "http://localhost:3000/exam/question/"+id;
    return this.httpClient.get(url);
  }
  ///exam
  getExam():Observable<any>
  {
    return this.httpClient.get(this.URLE);
  }
  getOneExam(id: string):Observable<any>
  {
    return this.httpClient.get("http://localhost:3000/exam/"+ id);
  }
  postExam(exam:Exam):Observable<any>
  {
    return this.httpClient.post(this.URLE, exam);
  }
  deleteExam(id:number):Observable<any>
  {
    const url = `${this.URLE}/${id}`; 
    return this.httpClient.delete(url);
  }
  getQuestionForExam(id: string):Observable<any>
  {
    return this.httpClient.get("http://localhost:3000/details/exam/"+ id);
  }
  putExam(id: string,exam: any){
    const url = `${this.URLE}/${id}`;
    console.log(url);
    return this.httpClient.put(url,exam);
  }
  ///detail
  postDetail(details: any):Observable<any>{
    return this.httpClient.post(this.URLD,details);
  }
  getDetail():Observable<any>{
    return this.httpClient.get(this.URLDE);
  }
  putDetails(id:string,insertQuestions: any,deleteQuestions: any):Observable<any>{
    const url = `${this.URLDE}/exam/${id}`;
    console.log(url);
    console.log({id, insertQuestions, deleteQuestions});
    return this.httpClient.put(url, {id, insertQuestions, deleteQuestions});

  }
  // Field
  getField():Observable<any>
  {
    return this.httpClient.get(this.URLF);
  }
  // Level
  getLevel():Observable<any>
  {
    return this.httpClient.get(this.URLL);
  }
  //get History
  getHistory():Observable<any>
  {
    return this.httpClient.get(this.URLH);
  }
  
}
