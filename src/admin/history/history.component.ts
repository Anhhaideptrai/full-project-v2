import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import {AdminService} from '../../admin/admin.service';
import {HistoryAdmin} from '../../Classes/historyAdmin';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponentAdmin implements OnInit {
  histories:HistoryAdmin[] = [];
  users = [];
  exam = [];
  constructor(private adminService: AdminService) { }

  ngOnInit(): void {
    this.adminService.getHistory().subscribe(data =>{
      this.histories = data;
      this.adminService.getUser().subscribe(data =>{
        this.users = data;
        this.adminService.getExam().subscribe(data =>{
          this.exam = data;
          console.log(this.exam)
          for(var i=0;i< this.histories.length;i++){
            for(var j=0;j< this.exam.length;j++){
              if(this.histories[i].exam == this.exam[j].id){
                this.histories[i].field = this.exam[j].field.major;
              }
            }
          }
          for(var i=0;i< this.histories.length;i++){
            for(var j=0;j< this.users.length;j++){
              if(this.histories[i].user == this.users[j].id){
                this.histories[i].name = this.users[j].name;
              }
            }
          }
          console.log(this.histories);
        });
      });
    });
  }

}
