import { AfterViewInit,Component, OnInit ,ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  delete: string;
  edit: string
  
}
const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H',delete: 'Xoa',edit: 'Sửa'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He',delete: 'xoa',edit: 'Sửa'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li',delete: 'xoa',edit: 'Sửa'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be',delete: 'xoa',edit: 'Sửa'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B',delete: 'xoa',edit: 'Sửa'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C',delete: 'xoa',edit: 'Sửa'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N',delete: 'xoa',edit: 'Sửa'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O',delete: 'xoa',edit: 'Sửa'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F',delete: 'xoa',edit: 'Sửa'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne',delete: 'xoa',edit: 'Sửa'},
  {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na',delete: 'xoa',edit: 'Sửa'},
  {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg',delete : 'xoa',edit: 'Sửa' },
];
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements  OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
    infor="Log Out"
  constructor(private breakpointObserver: BreakpointObserver) {}
  ngOnInit(): void {
  }
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol','delete','edit'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
}



