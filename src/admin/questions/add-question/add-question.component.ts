import {Quizzes} from '../../../Classes/quizzes';
import {Field} from '../../../Classes/field';
import {Level} from '../../../Classes/level';
import { Component, OnInit } from '@angular/core';
import { from, Observable, Subscriber} from 'rxjs';
import {AdminService} from '../../admin.service';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})

export class AddQuestionComponent implements OnInit {
  questions = [];
  questionsCopy:Quizzes[  ] = [];
  questionsCheck:Quizzes[  ] = [];
  show = true;
  hide = false;
  fields: Field[] = [];
  levels: Level[] = []; 
  constructor(private adminService: AdminService) { 
    
  }
  ngOnInit(): void {
    
  }
  uploadExcel(e) {
    try{
    const fileName = e.target.files[0].name;
    import('xlsx').then(xlsx => {
      let workBook = null;
      let jsonData = null;
      const reader = new FileReader();
      // const file = ev.target.files[0];
      reader.onload = (event) => {
        const data = reader.result;
        workBook = xlsx.read(data, { type: 'binary' });
        jsonData = workBook.SheetNames.reduce((initial, name) => {
          const sheet = workBook.Sheets[name];
          initial[name] = xlsx.utils.sheet_to_json(sheet);
          return initial;
        }, {});
        this.questions = jsonData[Object.keys(jsonData)[0]];
      };
      reader.readAsBinaryString(e.target.files[0]);
    });
    
  }catch(e){
     console.log('error', e);
  }
 }
  ADD(){
    this.show = false;
    this.hide = true;
    this.adminService.getField().subscribe(data => { this.fields = [...data]
      this.adminService.getLevel().subscribe(data => { this.levels = [...data]
        this.questions.map(question=>{
          const field = this.fields.find(field => field.major.toLocaleLowerCase() == question.field.toLocaleLowerCase()); 
          const level = this.levels.find(level =>level.label.toLocaleLowerCase() == question.level.toLocaleLowerCase()); 
          question.field = field;
          question.level = level;
        });
        this.adminService.getQuestion().subscribe(res => {
          this.questionsCopy = res;
          const checkRemain =  this.questions.filter(rs =>{
          const check  = this.questionsCopy.find(rsl => rs.content == rsl.content);
          if(check){
            return true;
          }
          else
            return false;
          }); 
          console.log(checkRemain);
          if (checkRemain.length > 0) {
            const errIndex = checkRemain.reduce((acc, cur) => {
              return [...acc, cur.STT];
            }, []);
            alert("The question already exists at Index: "+ errIndex.toString() +". Please Try Again!!");
            window.location.href ="/admin/addQuestion";
            return;
          }
          else{
            window.location.href ="/admin/manageQuestions";
            this.adminService.postQuestions(this.questions).subscribe();
          }
        });
      }); 
    });
  }
}
