import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,ParamMap} from '@angular/router'
import { AdminService } from 'src/admin/admin.service';
import { Quizzes } from 'src/Classes/quizzes';

@Component({
  selector: 'app-edit-question',
  templateUrl: './edit-question.component.html',
  styleUrls: ['./edit-question.component.css']
})
export class EditQuestionComponent implements OnInit {
  id ="";
  error: false;
  content = "";
  solutiona = "";
  solutionb = "";
  solutionc = "";
  solutiond = "";
  correct = "";
  field = null;
  level= null;
  question: Quizzes;
  constructor(private route: ActivatedRoute,private adminService: AdminService ) { }
  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) =>{
      this.id = params.get('id');
      this.adminService.getOneQuesion(this.id).subscribe(data =>{
        this.question = data;
        this.content = this.question.content;
        this.solutiona = this.question.solutionA;
        this.solutionb = this.question.solutionB;
        this.solutionc = this.question.solutionC;
        this.solutiond = this.question.solutionD;
        this.correct = this.question.correct;
        this.field = this.question.field.id;
        this.level = this.question.level.id;
      });
    });
  }
  onSubmit(formSignIn){
    console.log(formSignIn.value);
    console.log(this.id);
    this.adminService.putQuestion(formSignIn.value,this.id).subscribe();
    window.location.href='/admin/manageQuestions';
  }

}
