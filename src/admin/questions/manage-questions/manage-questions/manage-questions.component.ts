import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../../admin.service';
import {Quizzes} from '../../../../Classes/quizzes';

@Component({
  selector: 'app-manage-questions',
  templateUrl: './manage-questions.component.html',
  styleUrls: ['./manage-questions.component.css']
})
export class ManageQuestionsComponent implements OnInit {
  p = null;
  constructor(private adminSevice: AdminService) { }
  questions: Quizzes[] = [] ;
  search: any;
  details= [];
  ngOnInit(): void {
    this.adminSevice.getQuestion().subscribe(data =>
       {this.questions = data;
        console.log(this.questions);
       });
  }
  deleteQuestion(id: string){
    this.adminSevice.getDetail().subscribe(res => {
      this.details = res;
      if( this.details.filter(e => e.question == id).length > 0){
        alert("This question already exists in the subject please try again later!!!");
        return;
      }
      this.adminSevice.delQuestion(id).subscribe();
      if(window.confirm(" Coffirm Delete?"))
      window.location.href="/admin/manageQuestions";
    });
  }
  Search(){
    if(this.search == ""){
      this.ngOnInit();
    }
    else{
      this.questions = this.questions.filter(res =>{
        return res.content.toLocaleLowerCase().match(this.search.toLocaleLowerCase());
      } );
    }
  }

}
