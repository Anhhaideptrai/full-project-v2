import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from '../register.component';
import { MaterialModule } from '../../materialModule/material.module';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { RouterModules } from './../../routerModule/router/router.module';

import { HttpClientModule } from '@angular/common/http';
import { RegisterAPIService } from './../register.service';

@NgModule({
  declarations: [ RegisterComponent ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule, 
    FormsModule,
    RouterModules,
    HttpClientModule
  ],
  providers:[RegisterAPIService]
})
export class RegisterModule { }
