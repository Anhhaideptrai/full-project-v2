import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterAPIService } from './../register/register.service';
import { User } from './../Classes/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  constructor(private router: Router, private api: RegisterAPIService) { }

  UserData: User[];

  ngOnInit(): void {
    this.api.getUser().subscribe(
      data => {this.UserData = data}
    );
  }

  //Regexception
  userFormControl = new FormControl('',[Validators.required ])
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('',[Validators.required])
  repasswordFormControl = new FormControl('',[Validators.required])

  userName : string = "";
  password : string = "";
  repassword : string = "";
  email : string = "";
  numauth:number;

  Register_Fnc(){
    var userObjToPost = new User();

    userObjToPost.name = this.userName;
    userObjToPost.email = this.email;
    userObjToPost.password = this.password;
    userObjToPost.phone ="";
    userObjToPost.role =false;
    userObjToPost.address ="";
    if(this.numauth <= 0){
      alert("Authenication Number must be >0")
    }
    else{
      userObjToPost.authentication = this.numauth;

      var userCheck= new User();
      for(let i =0 ; i< this.UserData.length; i++)
      {
        if(this.UserData[i].name == this.userName){
          userCheck.name = this.UserData[i].name;
        }
      }
      if(this.repassword == this.password && this.password != "")
      {
        if(userCheck.name != null)
        {
          alert("exist")
        }
        else
        {
          this.api.postUser(userObjToPost).subscribe(data=>{data})
          if(window.confirm("Login Now ?"))
          {
            this.router.navigate[''];
          }else
          {
            window.location.reload();
          } 
        }
      }
      else{
        alert("Wrong")
      }
    }   
  }
}
