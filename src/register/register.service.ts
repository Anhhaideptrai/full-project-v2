import { Injectable } from '@angular/core';
import { User } from './../Classes/user';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class RegisterAPIService {
  
  URL: string = "http://localhost:3000/user";
  constructor(private httpClient: HttpClient) { }

  getUser():Observable<any>
  {
    return this.httpClient.get(this.URL);
  }
  
  postUser(user:User):Observable<any>
  {
    return this.httpClient.post(this.URL,user)
  }
}
