import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { RegisterModule } from '../register/registerModule/register.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../materialModule/material.module';
import { CountdownModule } from 'ngx-countdown';
import { RouterModules } from './../routerModule/router/router.module';
import { HomeModule } from './../Home/home/home.module';
import { AdminModule } from './../admin/admin/admin.module';
import { UserModuleModule } from '../user/user-module/user-module.module';
import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    LayoutModule,
    FormsModule,
    RouterModules,
    CountdownModule,
    RegisterModule,
    HomeModule,
    AdminModule,
    UserModuleModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
