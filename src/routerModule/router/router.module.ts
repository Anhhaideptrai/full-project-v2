import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomeMainComponent } from './../../Home/home-main/home-main.component';
import { NavUserComponent } from './../../user/nav-user/nav-user.component';
import { HistoryComponent } from './../../user/history/history.component';
import { UserInforComponent } from './../../user/user-infor/user-infor.component';
import { UserTestChooseComponent } from './../../user/user-test-choose/user-test-choose.component';
import { DoTestComponent } from './../../user/do-test/do-test.component';
import { AdminComponent } from './../../admin/admin.component';
import { ManageAccountsComponent } from './../../admin/accounts/manage-accounts/manage-accounts.component';
import { EditAccountComponent } from './../../admin/accounts/edit-account/edit-account.component';
import { DeleteAccountComponent } from './../../admin/accounts/delete-account/delete-account.component';
import { AddAccountComponent } from './../../admin/accounts/add-account/add-account.component';
import { ManageTestComponent } from './../../admin/test/manage-test/manage-test.component';
import { AddTestComponent } from './../../admin/test/add-test/add-test.component';
import { EditTestComponent } from './../../admin/test/edit-test/edit-test.component';
import { DeleteTestComponent } from './../../admin/test/delete-test/delete-test.component';
import { RegisterComponent } from 'src/register/register.component';
import { NavBlogComponent } from './../../user/nav-blog/nav-blog.component';
import { ManageQuestionsComponent } from 'src/admin/questions/manage-questions/manage-questions/manage-questions.component';
import { AddQuestionComponent } from 'src/admin/questions/add-question/add-question.component';
import { EditQuestionComponent } from 'src/admin/questions/edit-question/edit-question.component';
import { DeleteQuestionComponent } from 'src/admin/questions/delete-question/delete-question.component';
import {HistoryComponentAdmin} from 'src/admin/history/history.component';
import { MarkComponent } from 'src/admin/history/mark/mark.component';

const routes: Routes=[
    { path: '', component: HomeMainComponent },
    
    { path: 'register', component: RegisterComponent },
    { path: 'history', component: HistoryComponent },
    { path: 'infor', component: UserInforComponent },
    { path: 'user',
    component: NavUserComponent,  
    children: [
      {
        path:'history',component:HistoryComponent
      },
      {
        path:'test-blog',component:NavBlogComponent
      },
      { path: 'information', component: UserInforComponent },
      { path: 'testchoose' , component: UserTestChooseComponent},
      {
        path:'dotest', component: DoTestComponent
      },
      ],
    },
    { 
      path: 'admin',
      component: AdminComponent,
      children:[{ 
          path: 'manageAccounts',
          component:ManageAccountsComponent    
        },
        { path: 'addAccount', 
        component: AddAccountComponent 
      },
        { 
          path: 'editAccount/:id',
          component:EditAccountComponent
        },
        { 
          path: 'deleteAccount/:id',
          component:DeleteAccountComponent
          },
        
        { 
          path: 'manageTest',
          component:ManageTestComponent
        },
        { 
          path: 'addTest',
          component:AddTestComponent
        },
        { 
          path: 'editTest/:id',
          component:EditTestComponent
        },
        { 
          path: 'deleteTest/:id',
          component:DeleteTestComponent
        },
        { 
          path: 'manageQuestions',
          component:ManageQuestionsComponent
        },
        { 
          path: 'addQuestion',
          component:AddQuestionComponent
        },
        { 
          path: 'editQuestion/:id',
          component:EditQuestionComponent
        },
        { 
          path: 'deleteQuestion/:id',
          component:DeleteQuestionComponent
        },
        {
          path: 'histories',
          component: HistoryComponentAdmin
        },
        {
          path: 'histories/:id',
          component: MarkComponent
        }
        
      ]
    
  }, 
    
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule]
})
export class RouterModules { }
