import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './../Home-Components/nav/nav.component';
import { FooterComponent } from './../Home-Components/footer/footer.component';
import { HomeMainComponent } from './../home-main/home-main.component';
import { MaterialModule } from './../../materialModule/material.module';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { RouterModules } from './../../routerModule/router/router.module';

import { HttpClientModule } from '@angular/common/http';
import { HomeAPIService } from './../home.service';

@NgModule({
  declarations: [NavComponent, FooterComponent , HomeMainComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule, 
    FormsModule,
    RouterModules,
    HttpClientModule,

  ],
  providers: [HomeAPIService]
})
export class HomeModule { }
