import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from './../Classes/user';
import { Observable } from 'rxjs';

@Injectable()
export class HomeAPIService {
  URL: string ="http://localhost:3000/user";
  constructor(private httpClient: HttpClient) { }

  getUser():Observable<any>
  {
    return this.httpClient.get(this.URL);
  }

  getUserWithUserName(name : string):Observable<any>
  {
    return this.httpClient.get(this.URL+"/name/"+name)  
  }

  putUserWithUserId(user:User):Observable<any>
  {
    return this.httpClient.put(this.URL+"/"+user.id, user);
  }

  dataUser = new User();

  setUser(user:User){
    this.dataUser = user
  }

  getUserInfor(){
    return this.dataUser;
  }
  
}
